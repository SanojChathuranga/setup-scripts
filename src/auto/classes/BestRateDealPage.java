package auto.classes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import setup.enumtypes.BestRateStartingFrom;

import com.common.Validators.CommonValidator;

public class BestRateDealPage {

	BestRateDealConfig dealConfig = new BestRateDealConfig();

	String countrytxt = "-";
	boolean countrytxtbox = false;
	boolean countrylookup = false;
	String citytxt = "-";
	boolean citytxtbox = false;
	boolean citylookup = false;
	String hoteltxt = "-";
	boolean hoteltxtbox = false;
	boolean hotellookup = false;

	// Best Rate Elements
	String bestrateStartFromtxt = "-";
	boolean bestrateStartFromBookDateRadbtn = false;
	String bestrateStartFromBookDatetxt = "-";
	boolean bestrateStartFromCheckinDateRadbtn = false;
	String bestrateStartFromCheckinDatetxt = "-";
	String bestratefromtxt = "-";
	boolean fromDay = false;
	boolean fromMonth = false;
	boolean fromYear = false;
	boolean fromImg = false;
	boolean toDay = false;
	boolean toMonth = false;
	boolean toYear = false;
	boolean toImg = false;
	String notetxt = "-";
	boolean notetxtbox = false;
	boolean bestrateSaveBtn = false;
	boolean bestratePromoApplied = false;
	// Saved best rate promo
	String brsavedNotes = "-";
	String brsavedDateBy = "-";
	String brsavedStartingDate = "-";
	String brsavedEndingDate = "-";

	// Deal Elements
	String dealNametxt = "-";
	boolean dealNametxtbox = false;
	String dealStartingFromtxt = "-";
	boolean dealStartFromBookDateRadbtn = false;
	String dealStartFromBookDatetxt = "-";
	boolean dealStartFromCheckinDateRadbtn = false;
	String dealStartFromCheckinDatetxt = "-";
	String dealSelectDate = "-";
	boolean dealSelectDateImg = false;
	boolean dealSelectDatesDisplayed = false;
	boolean dealPromoApplied = false;
	// Saved deal promo
	String dsavedNotes = "-";
	String dsavedDateBy = "-";
	String dsavedStartingDate = "-";
	String dsavedEndingDate = "-";

	boolean status = false;
	HashMap<String, String> properties = new HashMap<>();

	// Default constructor
	public BestRateDealPage() {

	}

	// Parameterized constrictor property map
	public BestRateDealPage(HashMap<String, String> properties) {
		this.properties = properties;
	}

	// Parameterized constructor property path
	public BestRateDealPage(String propertyPath) {
		try {
			this.properties = readpropeties(propertyPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setPage(WebDriver driver) {

		if (driver.findElement(By.xpath(properties.get("PromoPg_CountryLookup_Label_xpath"))).isDisplayed()) {
			countrytxt = driver.findElement(By.xpath(properties.get("PromoPg_CountryLookup_Label_xpath"))).getText().trim();
		}
		if (driver.findElement(By.id(properties.get("PromoPg_CountryLookup_TxtBox_id"))).isDisplayed()) {
			countrytxtbox = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_CountryLookup_icon_id"))).isDisplayed()) {
			countrylookup = true;
		}
		if (driver.findElement(By.xpath(properties.get("PromoPg_CityLookup_Label_xpath"))).isDisplayed()) {
			citytxt = driver.findElement(By.xpath(properties.get("PromoPg_CityLookup_Label_xpath"))).getText().trim();
		}
		if (driver.findElement(By.id(properties.get("PromoPg_CityLookup_TxtBox_id"))).isDisplayed()) {
			citytxtbox = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_CityLookup_icon_id"))).isDisplayed()) {
			citylookup = true;
		}
		if (driver.findElement(By.xpath(properties.get("PromoPg_HotelLookup_Label_xpath"))).isDisplayed()) {
			hoteltxt = driver.findElement(By.xpath(properties.get("PromoPg_HotelLookup_Label_xpath"))).getText().trim();
		}
		if (driver.findElement(By.id(properties.get("PromoPg_HotelLookup_TxtBox_id"))).isDisplayed()) {
			hoteltxtbox = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_HotelLookup_icon_id"))).isDisplayed()) {
			hotellookup = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateStartFrom_Label_id"))).isDisplayed()) {
			bestrateStartFromtxt = driver.findElement(By.id(properties.get("PromoPg_BRateStartFrom_Label_id"))).getText().trim();
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateStartFromBookingDate_RadBtn_id"))).isDisplayed()) {
			bestrateStartFromBookDateRadbtn = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateStartFromBookingDate_Label_id"))).isDisplayed()) {
			bestrateStartFromBookDatetxt = driver.findElement(By.id(properties.get("PromoPg_BRateStartFromBookingDate_Label_id"))).getText().trim();
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateStartFromCheckingDate_RadBtn_id"))).isDisplayed()) {
			bestrateStartFromCheckinDateRadbtn = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateStartFromCheckingDate_Label_id"))).isDisplayed()) {
			bestrateStartFromCheckinDatetxt = driver.findElement(By.id(properties.get("PromoPg_BRateStartFromCheckingDate_Label_id"))).getText().trim();
		}
		if (driver.findElement(By.xpath(properties.get("PromoPg_BRateFromDate_Label_xpath"))).isDisplayed()) {
			bestratefromtxt = driver.findElement(By.xpath(properties.get("PromoPg_BRateFromDate_Label_xpath"))).getText().trim();
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateFromDay_DrpDwn_id"))).isDisplayed()) {
			fromDay = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateFromMonth_DrpDwn_id"))).isDisplayed()) {
			fromMonth = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateFromYear_DrpDwn_id"))).isDisplayed()) {
			fromYear = true;
		}
		if (driver.findElement(By.xpath(properties.get("PromoPg_BRateFrom_DatePicker_xpath"))).isDisplayed()) {
			fromImg = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateToDay_DrpDwn_id"))).isDisplayed()) {
			toDay = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateToMonth_DrpDwn_id"))).isDisplayed()) {
			toMonth = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateToYear_DrpDwn_id"))).isDisplayed()) {
			toYear = true;
		}
		if (driver.findElement(By.xpath(properties.get("PromoPg_BRateTo_DatePicker_xpath"))).isDisplayed()) {
			toImg = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateNote_Label_id"))).isDisplayed()) {
			notetxt = driver.findElement(By.id(properties.get("PromoPg_BRateNote_Label_id"))).getText().trim();
		}
		if (driver.findElement(By.id(properties.get("PromoPg_BRateNote_TxtBox_id"))).isDisplayed()) {
			notetxtbox = true;
		}
		if (driver.findElement(By.xpath(properties.get("PromoPg_BRateSave_Btn_xpath"))).isDisplayed()) {
			bestrateSaveBtn = true;
		}

		// Deals Elements
		if (driver.findElement(By.xpath(properties.get("PromoPg_DealName_Label_xpath"))).isDisplayed()) {
			dealNametxt = driver.findElement(By.xpath(properties.get("PromoPg_DealName_Label_xpath"))).getText().trim();
		}
		if (driver.findElement(By.id(properties.get("PromoPg_DealName_TxtBox_id"))).isDisplayed()) {
			dealNametxtbox = true;
		}
		if (driver.findElement(By.xpath(properties.get("PromoPg_DealStartFrom_Label_xpath"))).isDisplayed()) {
			dealStartingFromtxt = driver.findElement(By.xpath(properties.get("PromoPg_DealStartFrom_Label_xpath"))).getText().trim();
		}
		if (driver.findElement(By.id(properties.get("PromoPg_DealStartFromBookingDate_RadBtn_id"))).isDisplayed()) {
			dealStartFromBookDateRadbtn = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_DealStartFromBookingDate_Label_id"))).isDisplayed()) {
			dealStartFromBookDatetxt = driver.findElement(By.id(properties.get("PromoPg_DealStartFromBookingDate_Label_id"))).getText().trim();
		}
		if (driver.findElement(By.id(properties.get("PromoPg_DealStartFromCheckingDate_RadBtn_id"))).isDisplayed()) {
			dealStartFromCheckinDateRadbtn = true;
		}
		if (driver.findElement(By.id(properties.get("PromoPg_DealStartFromCheckingDate_Label_id"))).isDisplayed()) {
			dealStartFromCheckinDatetxt = driver.findElement(By.id(properties.get("PromoPg_DealStartFromCheckingDate_Label_id"))).getText().trim();
		}
		if (driver.findElement(By.xpath(properties.get("PromoPg_DealSelectDate_Label_xpath"))).isDisplayed()) {
			dealSelectDate = driver.findElement(By.xpath(properties.get("PromoPg_DealSelectDate_Label_xpath"))).getText().trim();
		}
		if (driver.findElement(By.xpath(properties.get("PromoPg_DealSelectDate_icon_xpath"))).isDisplayed()) {
			dealSelectDateImg = true;
		}

	}

	public void validatePageObjects(WebDriver driver, StringBuffer ReportPrinter) {

		this.setPage(driver);
		int Reportcount = 1;

		ReportPrinter.append("<span><center><p class='Hedding0'>Best Rate Deals Setup Page Validation</p></center></span>");
		ReportPrinter.append("<table style=width:100%>" + "<tr><th>Test Case</th>" + "<th>Test Description</th>" + "<th>Expected Result</th>" + "<th>Actual Result</th>" + "<th>Test Status</th></tr>");

		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_CountryLookup_Label_expected").trim(), countrytxt, "Country Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(countrytxtbox, "Country Text Box", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(countrylookup, "Country Lookup Icon", ReportPrinter, Reportcount);
		Reportcount++;

		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_CityLookup_Label_expected").trim(), citytxt, "City Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(citytxtbox, "City Text Box", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(citylookup, "City Lookup Icon", ReportPrinter, Reportcount);
		Reportcount++;

		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_HotelLookup_Label_expected").trim(), hoteltxt, "Hotel Name Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(hoteltxtbox, "Hotel Name Text Box", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(hotellookup, "Hotel Name Lookup Icon", ReportPrinter, Reportcount);
		Reportcount++;

		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_BRateStartFrom_Label_expected").trim(), bestrateStartFromtxt, "Best Rate Start From Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(bestrateStartFromBookDateRadbtn, "Booking Date Radio Button", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_BRateStartFromBookingDate_Label_expected").trim(), bestrateStartFromBookDatetxt, "Best Rate Start From Booking Date Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(bestrateStartFromCheckinDateRadbtn, "Checking date radio button", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_BRateStartFromCheckingDate_Label_expected").trim(), bestrateStartFromCheckinDatetxt, "Best Rate Start From Checking Date Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_BRateFromDate_Label_expected").trim(), bestratefromtxt, "Best Rate Start From Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(fromDay, "From Day Drop Down", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(fromMonth, "From Motnh Drop Down", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(fromYear, "From Year Drop Down", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(fromImg, "From Date Calender Icon", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(toDay, "To Day Drop Down", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(toMonth, "To Month Drop Down", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(toYear, "To Year Drop Down", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(toImg, "To Date Calender Icon", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_BRateNote_Label_expected").trim(), notetxt, "Best Rate Note Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(notetxtbox, "Note Text Box ", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(bestrateSaveBtn, "Best Rate Save Button ", ReportPrinter, Reportcount);
		Reportcount++;

		// Deal Validation
		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_DealName_Label_expected").trim(), dealNametxt, "Deal Name Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(dealNametxtbox, "Deal Name Text Box", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_DealStartFrom_Label_expected").trim(), dealStartingFromtxt, "Deal Start From Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(dealStartFromBookDateRadbtn, "Deal Start From Booking Date Radio Button", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_DealStartFromBookingDate_Label_expected").trim(), dealStartFromBookDatetxt, "Deal Name Radio Booking Date Button Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(dealStartFromCheckinDateRadbtn, "Deal Start From Checkin Date Radio Button", ReportPrinter, Reportcount);
		Reportcount++;
		try {
			status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_DealStartFromCheckinDate_Label_expected").trim(), dealStartFromCheckinDatetxt, "Deal Name Radio Checkin Date Button Label", ReportPrinter, Reportcount);
			Reportcount++;
		} catch (Exception e) {
			e.printStackTrace();
		}
		status = CommonValidator.compareStringEqualsIgnoreCase(properties.get("PromoPg_DealSelectDate_Label_xpath_expected").trim(), dealSelectDate, "Deal Select Dates Label", ReportPrinter, Reportcount);
		Reportcount++;
		status = CommonValidator.elementAvailable(dealSelectDateImg, "Deal Select Date Calander Icon", ReportPrinter, Reportcount);
		Reportcount++;

		ReportPrinter.append("</table>");
		ReportPrinter.append("<br><br><br>");
	}

	public void setConfig(WebDriver driver, BestRateDealConfig config) {
		try {
			this.dealConfig = config;

			driver.findElement(By.id(properties.get("PromoPg_CountryLookup_TxtBox_id"))).clear();
			driver.findElement(By.id(properties.get("PromoPg_CountryLookup_TxtBox_id"))).sendKeys(dealConfig.getCountry().trim());
			driver.findElement(By.id(properties.get("PromoPg_CountryLookup_icon_id"))).click();
			driver.switchTo().frame("lookup");
			Thread.sleep(3000);
			ArrayList<WebElement> cntry = new ArrayList<WebElement>(driver.findElements(By.className("rowcommonstyles")));
			for (int i = 0; i < cntry.size(); i++) {
				String c = cntry.get(i).findElements(By.tagName("td")).get(0).getText().trim();
				System.out.println(c);
				System.out.println(dealConfig.getCountry().trim());
				if (dealConfig.getCountry().trim().contains(c) || c.contains(dealConfig.getCountry().trim())) {
					cntry.get(i).click();
					break;
				}
			}
			driver.switchTo().defaultContent();

			driver.findElement(By.id(properties.get("PromoPg_CityLookup_TxtBox_id"))).clear();
			driver.findElement(By.id(properties.get("PromoPg_CityLookup_TxtBox_id"))).sendKeys(dealConfig.getCity().trim());
			driver.findElement(By.id(properties.get("PromoPg_CityLookup_icon_id"))).click();
			driver.switchTo().frame("lookup");
			Thread.sleep(3000);
			ArrayList<WebElement> cty = new ArrayList<WebElement>(driver.findElements(By.className("rowcommonstyles")));
			for (int i = 0; i < cty.size(); i++) {
				String c = cty.get(i).findElements(By.tagName("td")).get(0).getText().trim();
				System.out.println(c);
				System.out.println(dealConfig.getCity().trim());
				if (dealConfig.getCity().trim().contains(c) || c.contains(dealConfig.getCity().trim())) {
					cty.get(i).click();
					break;
				}
			}
			driver.switchTo().defaultContent();

			driver.findElement(By.id(properties.get("PromoPg_HotelLookup_TxtBox_id"))).clear();
			driver.findElement(By.id(properties.get("PromoPg_HotelLookup_TxtBox_id"))).sendKeys(dealConfig.getHotel().trim());
			driver.findElement(By.id(properties.get("PromoPg_HotelLookup_icon_id"))).click();
			driver.switchTo().frame("lookup");
			Thread.sleep(3000);
			ArrayList<WebElement> htl = new ArrayList<WebElement>(driver.findElements(By.className("rowcommonstyles")));
			for (int i = 0; i < htl.size(); i++) {
				String c = htl.get(i).findElements(By.tagName("td")).get(0).getText().trim();
				System.out.println(c);
				System.out.println(dealConfig.getHotel().trim());
				if (dealConfig.getHotel().trim().contains(c) || c.contains(dealConfig.getHotel().trim())) {
					htl.get(i).click();
					break;
				}
			}
			driver.switchTo().defaultContent();

			if (dealConfig.isBestRate()) {
			}
			if (dealConfig.isDeal()) {
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public void setConfiguration(WebDriver driver, BestRateDealConfig config) {

		try {

			this.dealConfig = config;
			setConfig(driver, dealConfig);

			driver.switchTo().defaultContent();

			if (dealConfig.isBestRate()) {
				if (dealConfig.getBestrateStartFrom().equals(BestRateStartingFrom.Booking_Date)) {
					driver.findElement(By.id(properties.get("PromoPg_BRateStartFromBookingDate_RadBtn_id"))).click();
				} else if (dealConfig.getBestrateStartFrom().equals(BestRateStartingFrom.CheckIn_Date)) {
					driver.findElement(By.id(properties.get("PromoPg_BRateStartFromCheckingDate_RadBtn_id"))).click();
				}

				new Select(driver.findElement(By.id(properties.get("PromoPg_BRateFromDay_DrpDwn_id")))).selectByVisibleText(dealConfig.getBestrateFromDay().trim());
				new Select(driver.findElement(By.id(properties.get("PromoPg_BRateFromMonth_DrpDwn_id")))).selectByVisibleText(dealConfig.getBestrateFromMonth().trim());
				new Select(driver.findElement(By.id(properties.get("PromoPg_BRateFromYear_DrpDwn_id")))).selectByVisibleText(dealConfig.getBestrateFromYear().trim());

				new Select(driver.findElement(By.id(properties.get("PromoPg_BRateToDay_DrpDwn_id")))).selectByVisibleText(dealConfig.getBestrateToDay().trim());
				new Select(driver.findElement(By.id(properties.get("PromoPg_BRateToMonth_DrpDwn_id")))).selectByVisibleText(dealConfig.getBestrateToMonth().trim());
				new Select(driver.findElement(By.id(properties.get("PromoPg_BRateToYear_DrpDwn_id")))).selectByVisibleText(dealConfig.getBestrateToYear().trim());

				driver.findElement(By.id(properties.get("PromoPg_BRateNote_TxtBox_id"))).sendKeys(dealConfig.getNote().trim());

				driver.findElement(By.xpath(properties.get("PromoPg_BRateSave_Btn_xpath"))).click();

				Thread.sleep(3000);
				if (driver.findElement(By.id("MainMsgBox_msgDisplayArea")).isDisplayed()) {
					String msg = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					if (msg.contains("Dates period(s) sholud not overlap")) {
						((JavascriptExecutor) driver).executeScript("javascript:closeMsgBox('MainMsgBox');");
						deleteConfigBestRate(driver);
						setConfig(driver, dealConfig);
						driver.findElement(By.xpath(properties.get("PromoPg_BRateSave_Btn_xpath"))).click();
						((JavascriptExecutor) driver).executeScript("javascript:closeDialogMsg(dialogMsgBox);");

					}
				}

				ArrayList<WebElement> rows = new ArrayList<WebElement>(driver.findElement(By.xpath(properties.get("PromoPg_BRateSaved_Table_xpath"))).findElements(By.tagName("tr")));
				if (rows.size() == 2) {

					ArrayList<WebElement> columns = new ArrayList<WebElement>(rows.get(1).findElements(By.tagName("td")));
					String[] list = new String[10];
					for (int u = 0; u < columns.size(); u++) {
						list[u] = columns.get(u).getText().trim();
					}

					if (list[1].contains(dealConfig.getNote().trim()) && list[2].contains(properties.get("PromoPg_BRateStartFromBookingDate_Label_expected").trim())
							&& list[3].contains(dealConfig.getBestrateFromDay().concat(dealConfig.getBestrateFromMonth().concat(dealConfig.getBestrateFromYear())))
							&& list[4].contains(dealConfig.getBestrateToDay().concat(dealConfig.getBestrateToMonth().concat(dealConfig.getBestrateToYear())))) {
						bestratePromoApplied = true;
					}
				}
			}
			if (dealConfig.isDeal()) {
				driver.findElement(By.id(properties.get("PromoPg_DealName_TxtBox_id"))).sendKeys(dealConfig.getDealName().trim());

				if (dealConfig.getDealStartFrom().equals(BestRateStartingFrom.Booking_Date)) {
					driver.findElement(By.id(properties.get("PromoPg_DealStartFromBookingDate_RadBtn_id"))).click();
				} else if (dealConfig.getDealStartFrom().equals(BestRateStartingFrom.CheckIn_Date)) {
					driver.findElement(By.id(properties.get("PromoPg_DealStartFromCheckingDate_RadBtn_id"))).click();
				}

				/*
				 * SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy"); Date frdate = null; Date todate = null; try { frdate = format.parse(
				 * dealConfig.getDealFromDay().concat("-").concat(dealConfig.getDealFromMonth()).concat("-").concat(dealConfig.getDealFromYear())); todate = format.parse(
				 * dealConfig.getDealToDay().concat(dealConfig.getDealToMonth()).concat(dealConfig.getDealToYear())); long diff = frdate.getTime() - todate.getTime(); int diffInDays = (int) diff /
				 * (1000 * 60 * 60 * 24); System.out.println(diffInDays);
				 * 
				 * } catch (ParseException e1) { e1.printStackTrace(); }
				 */

				// Deal Date Selected
				driver.findElement(By.xpath(properties.get("PromoPg_DealSelectDate_icon_xpath").trim())).click();
				boolean nav = true;
				do {
					String dealdate = driver.findElement(By.id("heading_dcal")).getText().trim();
					if (dealdate.contains(dealConfig.getDealFromMonth()) || dealdate.contains(dealConfig.getDealToMonth()) && dealdate.contains(dealConfig.getDealFromYear())) {

						int intfrm = Integer.parseInt(dealConfig.getDealFromDay());
						int intto = Integer.parseInt(dealConfig.getDealToDay());
						int count = intto - intfrm;
						for (int y = 0; y <= count; y++) {
							ArrayList<WebElement> cells = new ArrayList<WebElement>(driver.findElement(By.xpath(properties.get("PromoPg_DealDateCells_xpath"))).findElements(By.tagName("td")));
							for (int h = 0; h < cells.size(); h++) {

								int jax = 0;
								try {
									jax = Integer.parseInt(cells.get(h).getText().trim());
								} catch (Exception e) {
									System.out.println(cells.get(h).getText().trim());
								}

								if ((intfrm + y) == jax) {
									cells.get(h).click();
									break;
								}
							}
						}

						((JavascriptExecutor) driver).executeScript("JavaScript:addSelDates_dcal();");
						nav = false;
					} else {
						((JavascriptExecutor) driver).executeScript("JavaScript:showNextMonth_dcal();");
					}

				} while (nav);

				if (!driver.findElement(By.xpath(properties.get("PromoPg_DealSelectedAllDates_Label_xpath"))).getText().equals("")) {
					dealSelectDatesDisplayed = true;
				}

				driver.findElement(By.xpath(properties.get("PromoPg_DealSave_Btn_xpath"))).click();

				if (driver.findElement(By.id("MainMsgBox_msgDisplayArea")).isDisplayed()) {
					String msg = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					if (msg.contains("Dates period(s) sholud not overlap")) {
						((JavascriptExecutor) driver).executeScript("javascript:closeMsgBox('MainMsgBox');");
						deleteConfigDeal(driver);
						setConfig(driver, dealConfig);
						driver.findElement(By.id(properties.get("PromoPg_DealName_TxtBox_id"))).sendKeys(dealConfig.getDealName().trim());
						driver.findElement(By.xpath(properties.get("PromoPg_DealSave_Btn_xpath"))).click();
						((JavascriptExecutor) driver).executeScript("javascript:closeDialogMsg(dialogMsgBox);");

					}
				}

				ArrayList<WebElement> rows = new ArrayList<WebElement>(driver.findElement(By.xpath(properties.get("PromoPg_BRateSaved_Table_xpath"))).findElements(By.tagName("tr")));
				if (rows.size() == 2) {

					ArrayList<WebElement> columns = new ArrayList<WebElement>(rows.get(1).findElements(By.tagName("td")));
					String[] list = new String[10];
					for (int u = 0; u < columns.size(); u++) {
						list[u] = columns.get(u).getText().trim();
					}

					if (list[1].contains(dealConfig.getNote().trim()) && list[2].contains(properties.get("PromoPg_BRateStartFromBookingDate_Label_expected").trim())
							&& list[3].contains(dealConfig.getBestrateFromDay().concat(dealConfig.getBestrateFromMonth().concat(dealConfig.getBestrateFromYear())))
							&& list[4].contains(dealConfig.getBestrateToDay().concat(dealConfig.getBestrateToMonth().concat(dealConfig.getBestrateToYear())))) {
						dealPromoApplied = true;
					}
				}

				((JavascriptExecutor) driver).executeScript("javascript:closeDialogMsg(dialogMsgBox);");
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HashMap<String, String> readpropeties(String path) throws IOException {
		FileInputStream fs = null;
		Properties prop = new Properties();
		HashMap<String, String> mymap = null;

		try {
			fs = new FileInputStream(new File(path));
			prop.load(fs);
			mymap = new HashMap<String, String>((Map) prop);
			return mymap;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return mymap;
	}

	public boolean deleteConfigBestRate(WebDriver driver) {
		boolean done = false;
		try {
			driver.findElement(By.id(properties.get("PromoPg_CountryLookup_TxtBox_id"))).clear();
			driver.findElement(By.id(properties.get("PromoPg_CountryLookup_TxtBox_id"))).sendKeys(dealConfig.getCountry().trim());
			driver.findElement(By.id(properties.get("PromoPg_CountryLookup_icon_id"))).click();
			driver.switchTo().frame("lookup");
			Thread.sleep(3000);
			ArrayList<WebElement> cntry = new ArrayList<WebElement>(driver.findElements(By.className("rowcommonstyles")));
			for (int i = 0; i < cntry.size(); i++) {
				String c = cntry.get(i).findElements(By.tagName("td")).get(0).getText().trim();
				System.out.println(c);
				System.out.println(dealConfig.getCountry().trim());
				if (dealConfig.getCountry().trim().contains(c) || c.contains(dealConfig.getCountry().trim())) {
					cntry.get(i).click();
					break;
				}
			}
			driver.switchTo().defaultContent();

			driver.findElement(By.id(properties.get("PromoPg_CityLookup_TxtBox_id"))).clear();
			driver.findElement(By.id(properties.get("PromoPg_CityLookup_TxtBox_id"))).sendKeys(dealConfig.getCity().trim());
			driver.findElement(By.id(properties.get("PromoPg_CityLookup_icon_id"))).click();
			driver.switchTo().frame("lookup");
			Thread.sleep(3000);
			ArrayList<WebElement> cty = new ArrayList<WebElement>(driver.findElements(By.className("rowcommonstyles")));
			for (int i = 0; i < cty.size(); i++) {
				String c = cty.get(i).findElements(By.tagName("td")).get(0).getText().trim();
				System.out.println(c);
				System.out.println(dealConfig.getCity().trim());
				if (dealConfig.getCity().trim().contains(c) || c.contains(dealConfig.getCity().trim())) {
					cty.get(i).click();
					break;
				}
			}
			driver.switchTo().defaultContent();

			driver.findElement(By.id(properties.get("PromoPg_HotelLookup_TxtBox_id"))).clear();
			driver.findElement(By.id(properties.get("PromoPg_HotelLookup_TxtBox_id"))).sendKeys(dealConfig.getHotel().trim());
			driver.findElement(By.id(properties.get("PromoPg_HotelLookup_icon_id"))).click();
			driver.switchTo().frame("lookup");
			Thread.sleep(3000);
			ArrayList<WebElement> htl = new ArrayList<WebElement>(driver.findElements(By.className("rowcommonstyles")));
			for (int i = 0; i < htl.size(); i++) {
				String c = htl.get(i).findElements(By.tagName("td")).get(0).getText().trim();
				System.out.println(c);
				System.out.println(dealConfig.getHotel().trim());
				if (dealConfig.getHotel().trim().contains(c) || c.contains(dealConfig.getHotel().trim())) {
					htl.get(i).click();
					break;
				}
			}
			ArrayList<WebElement> rows = new ArrayList<WebElement>(driver.findElement(By.xpath(properties.get("PromoPg_BRateSaved_Table_xpath"))).findElements(By.tagName("tr")));
			if (rows.size() >= 2) {
				ArrayList<WebElement> columns = new ArrayList<WebElement>(rows.get(1).findElements(By.tagName("td")));
				columns.get(6).click();
				driver.findElement(
						By.xpath("/html/body/table/tbody/tr[2]/td/form/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/img"))
						.click();
				((JavascriptExecutor) driver).executeScript("javascript:closeDialogMsg(dialogMsgBox);");
				done = true;
			} else {
				done = false;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return done;
	}

	public boolean deleteConfigDeal(WebDriver driver) {
		boolean done = false;

		try {

			driver.findElement(By.id(properties.get("PromoPg_CountryLookup_TxtBox_id"))).clear();
			driver.findElement(By.id(properties.get("PromoPg_CountryLookup_TxtBox_id"))).sendKeys(dealConfig.getCountry().trim());
			driver.findElement(By.id(properties.get("PromoPg_CountryLookup_icon_id"))).click();
			driver.switchTo().frame("lookup");
			Thread.sleep(3000);
			ArrayList<WebElement> cntry = new ArrayList<WebElement>(driver.findElements(By.className("rowcommonstyles")));
			for (int i = 0; i < cntry.size(); i++) {
				String c = cntry.get(i).findElements(By.tagName("td")).get(0).getText().trim();
				System.out.println(c);
				System.out.println(dealConfig.getCountry().trim());
				if (dealConfig.getCountry().trim().contains(c) || c.contains(dealConfig.getCountry().trim())) {
					cntry.get(i).click();
					break;
				}
			}
			driver.switchTo().defaultContent();

			driver.findElement(By.id(properties.get("PromoPg_CityLookup_TxtBox_id"))).clear();
			driver.findElement(By.id(properties.get("PromoPg_CityLookup_TxtBox_id"))).sendKeys(dealConfig.getCity().trim());
			driver.findElement(By.id(properties.get("PromoPg_CityLookup_icon_id"))).click();
			driver.switchTo().frame("lookup");
			Thread.sleep(3000);
			ArrayList<WebElement> cty = new ArrayList<WebElement>(driver.findElements(By.className("rowcommonstyles")));
			for (int i = 0; i < cty.size(); i++) {
				String c = cty.get(i).findElements(By.tagName("td")).get(0).getText().trim();
				System.out.println(c);
				System.out.println(dealConfig.getCity().trim());
				if (dealConfig.getCity().trim().contains(c) || c.contains(dealConfig.getCity().trim())) {
					cty.get(i).click();
					break;
				}
			}
			driver.switchTo().defaultContent();

			driver.findElement(By.id(properties.get("PromoPg_HotelLookup_TxtBox_id"))).clear();
			driver.findElement(By.id(properties.get("PromoPg_HotelLookup_TxtBox_id"))).sendKeys(dealConfig.getHotel().trim());
			driver.findElement(By.id(properties.get("PromoPg_HotelLookup_icon_id"))).click();
			driver.switchTo().frame("lookup");
			Thread.sleep(3000);
			ArrayList<WebElement> htl = new ArrayList<WebElement>(driver.findElements(By.className("rowcommonstyles")));
			for (int i = 0; i < htl.size(); i++) {
				String c = htl.get(i).findElements(By.tagName("td")).get(0).getText().trim();
				System.out.println(c);
				System.out.println(dealConfig.getHotel().trim());
				if (dealConfig.getHotel().trim().contains(c) || c.contains(dealConfig.getHotel().trim())) {
					htl.get(i).click();
					break;
				}
			}
			ArrayList<WebElement> rows = new ArrayList<WebElement>(driver.findElement(By.xpath(properties.get("PromoPd_DealSaved_Table_xpath"))).findElements(By.tagName("tr")));
			if (rows.size() == 2) {
				ArrayList<WebElement> columns = new ArrayList<WebElement>(rows.get(1).findElements(By.tagName("td")));
				columns.get(5).click();
				driver.findElement(
						By.xpath("/html/body/table/tbody/tr[2]/td/form/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/img"))
						.click();
				((JavascriptExecutor) driver).executeScript("javascript:closeDialogMsg(dialogMsgBox);");
				done = true;
			} else {
				done = false;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return done;
	}

}
