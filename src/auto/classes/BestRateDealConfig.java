package auto.classes;

import java.util.ArrayList;
import java.util.Map;

import setup.enumtypes.BestRateStartingFrom;
import setup.utilities.*;

public class BestRateDealConfig {

	// Page Default Elements
	String country = "-";	
	String city = "-";
	String hotel = "-";
	BestRateStartingFrom bestrateStartFrom = null;
	String bestrateFromDay = "";
	String bestrateFromMonth = "";
	String bestrateFromYear = "";
	String bestrateToDay = "";
	String bestrateToMonth = "";
	String bestrateToYear = "";
	String note = "-";
	String dealName = "-";
	BestRateStartingFrom dealStartFrom = null;
	String dealFromDay = "";
	String dealFromMonth = "";
	String dealFromYear = "";
	String dealToDay = "";
	String dealToMonth = "";
	String dealToYear = "";
	boolean isBestRate = false;
	boolean isDeal = false;
	
	
	public void setConfigurations(String XLPath){
		
		ArrayList<Map<Integer, String>> XLtestData		= new ArrayList<Map<Integer,String>>();
		ExcelReader Reader	= new ExcelReader();
		XLtestData 			= Reader.init(XLPath);
		
		for (int y = 0; y<1; y++) 
		{
			Map<Integer, String> Sheet = XLtestData.get(y);
			
			for(int x = 0; x<Sheet.size(); x++)
			{
				String[] testData = Sheet.get(x).split(",");
				
				this.setCountry(testData[1].trim());
				this.setCity(testData[2].trim());
				this.setHotel(testData[3].trim());
				
				if(testData[0].trim().equalsIgnoreCase("true"))
				{
					isBestRate = true;
					
					if(testData[4].trim().equalsIgnoreCase("Booking_Date"))
					{
						this.setBestrateStartFrom(BestRateStartingFrom.Booking_Date);
					}
					else if(testData[4].trim().equalsIgnoreCase("CheckIn_Date"))
					{
						this.setBestrateStartFrom(BestRateStartingFrom.CheckIn_Date);
					}
					
					String fromdate = testData[5].trim();
					String[] fromdateArr = fromdate.split("-");
					this.setBestrateFromDay(fromdateArr[0].trim());
					this.setBestrateFromMonth(fromdateArr[1].trim());
					this.setBestrateFromYear(fromdateArr[2].trim());
					
					String todate = testData[6].trim();
					String[] todateArr = todate.split("-");
					this.setBestrateToDay(todateArr[0].trim());
					this.setBestrateToMonth(todateArr[1].trim());
					this.setBestrateToYear(todateArr[2].trim());
					
					this.setNote(testData[7].trim());
				}
				
				//Deal
				if(testData[8].trim().equalsIgnoreCase("true"))
				{
					isDeal = true;
					
					this.setDealName(testData[9].trim());
					if(testData[10].trim().equalsIgnoreCase("Booking_Date"))
					{
						this.setDealStartFrom(BestRateStartingFrom.Booking_Date);
					}
					else if(testData[10].trim().equalsIgnoreCase("CheckIn_Date"))
					{
						this.setDealStartFrom(BestRateStartingFrom.CheckIn_Date);
					}
					
					String fromdate = testData[11].trim();
					String[] fromdateArr = fromdate.split("-");
					this.setDealFromDay(fromdateArr[0].trim());
					this.setDealFromMonth(fromdateArr[1].trim());
					this.setDealFromYear(fromdateArr[2].trim());
					
					String todate = testData[12].trim();
					String[] todateArr = todate.split("-");
					this.setDealToDay(todateArr[0].trim());
					this.setDealToMonth(todateArr[1].trim());
					this.setDealToYear(todateArr[2].trim());
				}
			}
		}
		
		
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getHotel() {
		return hotel;
	}

	public void setHotel(String hotel) {
		this.hotel = hotel;
	}

	public BestRateStartingFrom getBestrateStartFrom() {
		return bestrateStartFrom;
	}

	public void setBestrateStartFrom(BestRateStartingFrom bestrateStartFrom) {
		this.bestrateStartFrom = bestrateStartFrom;
	}

	public String getBestrateFromDay() {
		return bestrateFromDay;
	}

	public void setBestrateFromDay(String bestrateFromDay) {
		this.bestrateFromDay = bestrateFromDay;
	}

	public String getBestrateFromMonth() {
		return bestrateFromMonth;
	}

	public void setBestrateFromMonth(String bestrateFromMonth) {
		this.bestrateFromMonth = bestrateFromMonth;
	}

	public String getBestrateFromYear() {
		return bestrateFromYear;
	}

	public void setBestrateFromYear(String bestrateFromYear) {
		this.bestrateFromYear = bestrateFromYear;
	}

	public String getBestrateToDay() {
		return bestrateToDay;
	}

	public void setBestrateToDay(String bestrateToDay) {
		this.bestrateToDay = bestrateToDay;
	}

	public String getBestrateToMonth() {
		return bestrateToMonth;
	}

	public void setBestrateToMonth(String bestrateToMonth) {
		this.bestrateToMonth = bestrateToMonth;
	}

	public String getBestrateToYear() {
		return bestrateToYear;
	}

	public void setBestrateToYear(String bestrateToYear) {
		this.bestrateToYear = bestrateToYear;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getDealName() {
		return dealName;
	}

	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public BestRateStartingFrom getDealStartFrom() {
		return dealStartFrom;
	}

	public void setDealStartFrom(BestRateStartingFrom dealStartFrom) {
		this.dealStartFrom = dealStartFrom;
	}

	public String getDealFromDay() {
		return dealFromDay;
	}

	public void setDealFromDay(String dealFromDay) {
		this.dealFromDay = dealFromDay;
	}

	public String getDealFromMonth() {
		return dealFromMonth;
	}

	public void setDealFromMonth(String dealFromMonth) {
		this.dealFromMonth = dealFromMonth;
	}

	public String getDealFromYear() {
		return dealFromYear;
	}

	public void setDealFromYear(String dealFromYear) {
		this.dealFromYear = dealFromYear;
	}

	public String getDealToDay() {
		return dealToDay;
	}

	public void setDealToDay(String dealToDay) {
		this.dealToDay = dealToDay;
	}

	public String getDealToMonth() {
		return dealToMonth;
	}

	public void setDealToMonth(String dealToMonth) {
		this.dealToMonth = dealToMonth;
	}

	public String getDealToYear() {
		return dealToYear;
	}

	public void setDealToYear(String dealToYear) {
		this.dealToYear = dealToYear;
	}

	public boolean isBestRate() {
		return isBestRate;
	}

	public void setBestRate(boolean isBestRate) {
		this.isBestRate = isBestRate;
	}

	public boolean isDeal() {
		return isDeal;
	}

	public void setDeal(boolean isDeal) {
		this.isDeal = isDeal;
	}

	
}
