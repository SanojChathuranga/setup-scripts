package auto.setups;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import auto.classes.BestRateDealConfig;
import auto.classes.BestRateDealPage;
import setup.utilities.*;

public class BestRateDealSetup {

	HashMap<String, String> propertyMap = new HashMap<String, String>();
	// String propertyPath = "../Flight_Reservation_Details/PropertiesWEBCMB.properties";
	String propertyPath = "PropertiesWEB.properties";
	// String propertyPath = "../Flight_Reservation_Details/PropertiesWEBDemo.properties";
	// String propertyPath = "../Flight_Reservation_Details/PropertiesWEB_Prod.properties";
	// String propertyPath = "../Flight_Reservation_Details/PropertiesWEBBeverly.properties";

	StringBuffer ReportPrinter = null;
	WebDriver driver = null;
	boolean login = false;
	ArrayList<Map<Integer, String>> XLtestData = new ArrayList<Map<Integer, String>>();
	int m = 1;
	int Reportcount = 1;
	int count = 1;

	@Before
	public void born() {
		ReportPrinter = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

		String css = "style.css";
		ReportPrinter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\""+css+"\"> </head>");
		ReportPrinter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Flight Reservation - Web (" + sdf.format(Calendar.getInstance().getTime()) + ")</p></div>");
		ReportPrinter.append("<body>");

		try {
			propertyMap = ReadProperties.readpropeties(propertyPath);
			// support = new SupportMethods(propertyMap);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ExcelReader Reader = new ExcelReader();
		// XLtestData = Reader.init(propertyMap.get("XLPath"));
	}

	@Test
	public void live() {
		SupportMethods s = new SupportMethods(propertyMap);
		try {

			File profile = new File("C:/Users/Sanoj/AppData/Roaming/Mozilla/Firefox/Profiles/t7v2fp3b.default-1417678199350");
			FirefoxProfile prof = new FirefoxProfile(profile);
			driver = new FirefoxDriver(prof);
			s.login(driver, "sanojC", "123456");

			//
			driver.get("http://dev3.rezg.net/rezbase_v3/hotels/inventoryandrates/BestRateDealOfTheDayPage.do?module=contract");
			BestRateDealConfig config = new BestRateDealConfig();
			config.setConfigurations("Promotion.xls");
			BestRateDealPage page = new BestRateDealPage("ProdPromoPage.properties");
			page.setPage(driver);
			page.validatePageObjects(driver, ReportPrinter);
			page.setConfiguration(driver, config);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@After
	public void die() {
		ReportPrinter.append("</body></html>");
		BufferedWriter bwr;
		try {
			bwr = new BufferedWriter(new FileWriter(new File("Reports/Flight Web Reservation.html")));
			bwr.write(ReportPrinter.toString());
			bwr.flush();
			bwr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		driver.quit();
	}
}
