package css;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class readCss {
	
	public String readCss()
	{
		BufferedReader br = null;
		String css = "";
		int i = 0 ;
		 
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader("reports\\style.css"));

			while ((sCurrentLine = br.readLine()) != null) {
				//System.out.println(sCurrentLine);
				if(i == 0)
				{
					css = "\"" + sCurrentLine + "\"";
				}
				else
				{
					css = css.concat(" + ").concat("\"" + sCurrentLine + "\"");
				}
				i++;
				//System.out.println(css);
			}
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		System.out.println(css);
		return css;
	}
}
